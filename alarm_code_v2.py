#valentina.novati@northwestern.edu
#himangshuneog@gmail.com
#Update: The code is made to read mysql database to get latest fridge values -Himangshu

# run with ./alarm_code.py

import pandas as pd
import numpy as np
import json
import datetime
import time
import os
import glob
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
import pymysql.cursors
from prettytable import PrettyTable
from datetime import datetime,timedelta

##Fill this detail
hostname = 'scdms-ipc1.dhcp.fnal.gov'#'cdms-monitor.tes-uoft.com' 'scdms-monitor.tes-uoft.com' 'scdms-ipc1.dhcp.fnal.gov'

################
def send_message(text_message):
  #client = WebClient(token=os.environ['SLACK_BOT_TOKEN'])
  client = WebClient(token=token['token'])


  try:
    response = client.chat_postMessage(channel='#test_alarm_code', text=text_message)
    #"This still works and the fridge is not working today! \n 2 line message")
    #assert response["message"]["text"] == "Hello Tina!"
  except SlackApiError as e:
    # You will get a SlackApiError if "ok" is False
    assert e.response["ok"] is False
    assert e.response["error"]  # str like 'invalid_auth', 'channel_not_found'
    print("Got an error: {e.response['error']}")
  return()


#def read_plcData(pathname_series):
#  data = pd.read_csv(pathname_series, delimiter = '\t', encoding = 'latin', index_col=False)
#  return data


def alarm_reset():
  if (setting[TEMP][TYPE][AL]['status']>0):
    setting[TEMP][TYPE][AL]['status']=0
    setting[TEMP][TYPE][AL]['time']=0
    update_json_settings()
    send_message("Cleared: "+setting[TEMP][TYPE][AL]['text'])
  return()


def alarm_minmax(alarmtype, val, minval, maxval):
  #print(val, minval, maxval)
  if((val>maxval)|(val<minval)):
    alarm_alarm()
    print("Alarm Triggered: {}".format(alarmtype))
  else:
    alarm_reset()
  return()

# I am printing the values that make trigger the alarm on the log file, this alarm triggers more than I would like...
def alarm_stale(lista):
  #print(lista)
  if (np.std(lista)<1e-9):
    print(lista, np.std(lista))
    alarm_alarm()
  else:
    alarm_reset()
  return()


def alarm_statuschange(lista, bit):
  #print(lista)
  if bit=="06":
    bitlist = []
    for i in lista:
      bitlist.append(compState(i, bit))
    #print(bitlist)
    if (np.std(bitlist)>0):
      alarm_alarm()
      print("Status change detected..")
    else:
      #print("Status not changed")
      alarm_reset()
  else:
    if (np.std(lista)>0):
      alarm_alarm()
      print("Status change detected..")
    else:
      alarm_reset()
    return()


def alarm_comparison(val1, val2, threshold):
  if (np.abs(val1- val2)>threshold):
    alarm_alarm()
    print("Threshold exceeded..")
  else:
    alarm_reset()
  return()

def alarm_crossing(data_ar, delta, crossing_point):
  #print(data_ar[0], data_ar[5], np.mean(data_ar), np.mean(data_ar)-crossing_point)
  if ((np.abs(np.mean(data_ar)-crossing_point)<delta)&(data_ar[0]>data_ar[5])):
    alarm_alarm()
  else:
    alarm_reset()     
  return()

def date2timestamp(lista):
  timestamp=datetime(lista[0], lista[1], lista[2], lista[3], lista[4], lista[5], 0)
  return(timestamp)

def readtime(date,heures):
  year=2000+int(date.split('/')[2])
  month=int(date.split('/')[0])
  day=int(date.split('/')[1])
  hour=int(heures.split(':')[0])
  minute=int(heures.split(':')[1])
  second=int(heures.split(':')[2])
  data_time=date2timestamp([year,month,day,hour,minute,second])
  return(data_time)

def update_json_settings():
  with open(config_file, "w") as outfile:
    json.dump(setting, outfile, indent=4)
  return()


def alarm_alarm():
  if (stat==0): #check that the alarm isn't already triggered
    setting[TEMP][TYPE][AL]['status']=1 #trigger the alarm
    setting[TEMP][TYPE][AL]['time']=[now.year,now.month, now.day, now.hour, now.minute, now.second]
    update_json_settings()
    print("Sending SLACK message: "+setting[TEMP][TYPE][AL]['text']+tag)
    send_message(setting[TEMP][TYPE][AL]['text']+tag)
  return()

def send_reminder(reminder_number):
  send_message("Reminder "+str(reminder_number)+": "+setting[TEMP][TYPE][AL]['text']+tag)
  if (reminder_number==3):
    setting[TEMP][TYPE][AL]['status']=0
  else:
    setting[TEMP][TYPE][AL]['status']=setting[TEMP][TYPE][AL]['status']+1
  update_json_settings()
  return()

def getTimeColumn(qTable):
    if qTable == 'leiden_ptcompressors':
       return 'timestamp'
    else:
       return 'Date'

def getstartdate(enddate,dt):
    return enddate-timedelta(minutes=dt)

def getdata(tableName, timeold):
   connection = pymysql.connect(host=hostname, user='remoteuser',password='SoupRmysql!', database='testplc', cursorclass=pymysql.cursors.DictCursor)
   with connection.cursor() as cursor:
      tCol = getTimeColumn(tableName)
      #midQuery = "SELECT "+tCol+" FROM "+tableName
      maxQuery = "SELECT MAX("+tCol+") FROM "+tableName
      cursor.execute(maxQuery)
      result = cursor.fetchall()
      enddate=result[0]["MAX("+tCol+")"]
      startdate = str(getstartdate(enddate,timeold)) #All data from 3 minutes earlier ~ 6 timestamps
      myDataQuery = "SELECT * FROM "+tableName+" WHERE ("+tCol+" > '"+startdate+"') ORDER by "+tCol+";"
      cursor.execute(myDataQuery)
      #cursor.execute(midQuery)
      result2 = cursor.fetchall()
      return result2


def compState(state, bit):
    if state is not None:
        rawbinstate = bin(state)
        listrawbinstate= list(rawbinstate)
        listrawbinstate.pop(0)
        listrawbinstate.pop(0)
        #print(listrawbinstate)
        statebinary32=[]
        blanknumbers = 32-len(listrawbinstate)
        for i in range(blanknumbers):
            statebinary32.append('0')
        for i in range(blanknumbers, 32):
            statebinary32.append(listrawbinstate[i-blanknumbers])
        #print(statebinary32)
        case_dict = {
                            "01":  statebinary32[31-0],#S6
                            "02":  statebinary32[31-1],#19
                            "03":  statebinary32[31-2],#15
                            "04":  statebinary32[31-3],#16
                            "05":  statebinary32[31-4],#AUX1
                            "06":  statebinary32[31-5],#18
                            "07":  statebinary32[31-6],#4
                            "08":  statebinary32[31-7],#AUX2
                            "09":  statebinary32[31-8],#5
                            "010":  statebinary32[31-9],#RESET
                            "011":  statebinary32[31-10],#AUX3
                            "012":  statebinary32[31-11],#9
                            "013":  statebinary32[31-12],#14
                            "015":  statebinary32[31-14],#13
                            "016":  statebinary32[31-15],#LED TEST
                            "018":  statebinary32[31-17],#12
                            "019":  statebinary32[31-18],#10
                            "021":  statebinary32[31-20],#11
                            "022":  statebinary32[31-21],#A0
                            "024":  statebinary32[31-23],#7
                            "025":  statebinary32[31-24],#S3
                            "027":  statebinary32[31-26],#6
                            "028":  statebinary32[31-27],#17
                            "030":  statebinary32[31-29],#8
                            "031":  statebinary32[31-30],#1
        }
        default_bit = "99"
        return int(case_dict.get(bit, default_bit))

#res = compState(11,'01')
#print(res)


homepath='/Users/himangshuneog/Desktop/slow_monitor_alarms/'
dataP = 'leiden_frontpanel_maxigauge' #Pressure data table
dataT = 'leiden_tempcontrol_avs' #Temperature data table
dataPT = 'leiden_ptcompressors' #PTcompressor data

dataQuery = getdata(dataT, 3)
dataQueryP = getdata(dataP, 3)
dataQueryPT = getdata(dataPT, 3)
dataQueryALL = dataQuery+dataQueryP+dataQueryPT
#print([dataset.get('R1') for dataset in dataQuery])

#print(dataQuery)
#print("Combined")
#print(dataQueryALL)
latestdata= dataQuery[len(dataQuery)-1]
latestdataP= dataQueryP[len(dataQueryP)-1]
latestdataPT= dataQueryPT[len(dataQueryPT)-1]

three_min_old_data= dataQuery[0]
three_min_old_dataP= dataQueryP[0]
three_min_old_dataPT= dataQueryPT[0]

longdataQuery = getdata(dataT, 10)
longdataQueryP = getdata(dataP, 10)
longdataQueryPT = getdata(dataPT, 10)
longdataQueryALL = longdataQuery+longdataQueryP+longdataQueryPT
#print(latestdata)
#print(latestdataP)
#print(latestdataPT)
#----------------------------------------------------------------
#loading alarm settings
config_file=homepath+'config_alarm.json'
with open(config_file) as json_file:
  setting=json.load(json_file)

#slack token
token_file=homepath+'slack_token.json'#'token4test.json' #'token.json'
with open(token_file) as json_file:
  token=json.load(json_file)
#print(token['token'])

#tagging people on slack
slack_tag_file=homepath+'slack_config.json'
with open(slack_tag_file) as json_file:
  tag_dict=json.load(json_file)
tag=" "
for person in tag_dict["slack_ID"].keys():
  tag = tag+tag_dict["slack_ID"][person] 


#send_message('Hello!')

if (setting['status']['ENABLE']=='True'):   

  #time now
  now = datetime.now()
  #time in data
  #heures=data['heures'][filelen]
  #day=data['date'][filelen]
  data_time=latestdata[getTimeColumn(dataT)]
  
  #alarm active test
  test_alarm_time=date2timestamp([now.year,now.month, now.day,9,0,0])
  if((now.weekday()==0)&((np.abs(now-test_alarm_time)).total_seconds()<30)):
    send_message("Weekly notification: alarms active")
    print("Weekly notification: alarms active")
  
  #cold or warm alarm set in the config file 
  TEMP=setting['status']['fridge_temp']
  numberofalarms = 0
  print("Current State of SNOLAB fridge is: ", TEMP)
  print("Current active alarms are:")
  t = PrettyTable(['alarm_name', 'alarm_meaning', 'alarm_active?(1=yes, 0=no)'])
  for TYPE in setting[TEMP]:
    for AL in setting[TEMP][TYPE]:
      #print('{:>}    {:<}    {:>}'.format(setting[TEMP][TYPE][AL]['name'], setting[TEMP][TYPE][AL]['text'], setting[TEMP][TYPE][AL]['active_state']))
      t.add_row([setting[TEMP][TYPE][AL]['name'], setting[TEMP][TYPE][AL]['text'], setting[TEMP][TYPE][AL]['active_state']])
  print(t)
  

  #check all the alarms in the config file
  
  for TYPE in setting[TEMP]:
    for AL in setting[TEMP][TYPE]:
      namestr=setting[TEMP][TYPE][AL]['name']
      stat=setting[TEMP][TYPE][AL]['status']
      time_last_alarm=setting[TEMP][TYPE][AL]['time']
      alarm_active=setting[TEMP][TYPE][AL]['active_state']
      #data_ar=data[namestr][filelen-5:].tolist()
      #data_ar_stale=data[namestr][filelen-30:].tolist()
      data_arn = [dataset.get(namestr) for dataset in dataQueryALL]
      data_ar=list()
      for x in data_arn:
          if x is not None:
              data_ar.append(x)
      #print("Start set")
      #print("Namestr is"+namestr)
      #print(data_ar)
      #print("End set")
      data_ar_stale_raw = [dataset.get(namestr) for dataset in longdataQueryALL]
      data_ar_stale = list()
      for x in data_ar_stale_raw:
          if x is not None:
              data_ar_stale.append(x)
      #print(len(data_ar))
      #cycle the various alarm type and alarm variables and check the states of alarms
      #if (stat==0):
      if (TYPE=='minmax_alarm' and alarm_active):
        minval=setting[TEMP][TYPE][AL]['min']
        maxval=setting[TEMP][TYPE][AL]['max']
        if(AL=='time update'):
          alarm_minmax(namestr, (now-data_time).total_seconds(), minval,maxval)
        else:
          alarm_minmax(namestr, np.mean(data_ar), minval,maxval)
      
      if ((TYPE=='alarm_stale' and alarm_active)):
        #print(data_ar_stale, np.std(data_ar_stale))
        alarm_stale(data_ar_stale)
      
      if (TYPE=='status_alarm' and alarm_active):
          if namestr=='State0':
              alarm_statuschange(data_ar, "06")#V18 is 06 bit for the map in this code
          else:
              alarm_statuschange(data_ar, "99")#Place holder for PT status change
      '''
      if (TYPE=='comparison_alarm' and alarm_active):
        namestr2=setting[TEMP][TYPE][AL]['name2']
        threshold=setting[TEMP][TYPE][AL]['threshold']

        data_ar2=[dataset.get(namestr2, None) for dataset in dataQueryALL]
        alarm_comparison(np.mean(data_ar), np.mean(data_ar2), threshold)
      '''
      if (TYPE=='crossing_alarm' and alarm_active):
        crossing_point=setting[TEMP][TYPE][AL]['cross']
        delta=setting[TEMP][TYPE][AL]['delta']
        alarm_crossing(data_ar, delta, crossing_point)
            
      if (stat>0):
        #send reminders
        time_from_last_alarm=(now-date2timestamp(time_last_alarm)).total_seconds()
        if ((time_from_last_alarm>600)&(stat==1)): #after 10 min
           send_reminder(stat)
           
        if ((time_from_last_alarm>3600)&(stat==2)): #after 1 hour
           send_reminder(stat)

        if ((time_from_last_alarm>28800)&(stat==3)): #after 8 hours
           send_reminder(stat)


