#! /usr/local/soft/pythonenv/env_alarms/bin/python
#himangshuneog@gmail.com
#Update: The code is made to read mysql database to get latest fridge values -Himangshu

# run with ./configure_alarm_system.py

import pandas as pd
import numpy as np
import json
import datetime
import time
import os
import glob
from slack_sdk import WebClient
from slack_sdk.errors import SlackApiError
import pymysql.cursors
from prettytable import PrettyTable
from datetime import datetime,timedelta

#alarm settings files
config_file='config_alarm.json'

with open(config_file) as json_file:
  setting=json.load(json_file)

if (setting['status']['ENABLE']=='True'):
  print("Alarm is ON")
  TEMP=setting['status']['fridge_temp']
  print("Current State of SNOLAB fridge is: ", TEMP)
  print("Current active alarms are:")
  t = PrettyTable(['alarm_name', 'alarm_meaning', 'alarm_active?(1=yes, 0=no)'])
  for TYPE in setting[TEMP]:
    for AL in setting[TEMP][TYPE]:
        t.add_row([setting[TEMP][TYPE][AL]['name'], setting[TEMP][TYPE][AL]['text'], setting[TEMP][TYPE][AL]['active_state']])
  print(t)
  text = input("Do you want to turn ON/OFF any particular alarm above? (1=yes, 0=no)")
  if int(text)==1:
    for TYPE in setting[TEMP]:
      for AL in setting[TEMP][TYPE]:
        text3 = input("Turn ON/OFF {} alarm? (1=yes, 0=no)".format(setting[TEMP][TYPE][AL]['name']))
        if int(text3)==1:
          if setting[TEMP][TYPE][AL]['active_state']==1:
            setting[TEMP][TYPE][AL]['active_state']=0
          else:
            setting[TEMP][TYPE][AL]['active_state']=1
  text2 = input("Do you want to change the alarm STATE? (1=yes, 0=no)")
  if int(text2)==1:
    choice = input("Choose alarm STATE: (0= alarm_cold, 1= alarm_warm)")
    if int(choice)==0:
      setting['status']['fridge_temp']= 'alarm_cold'
    else:
      setting['status']['fridge_temp']= 'alarm_warm'

  text4 = input("Do you want to turn the alarm system OFF? (1=yes, 0=no)")
  if int(text4)==1:
    setting['status']['ENABLE']='False'


elif (setting['status']['ENABLE']=='False'):
  print("Alarm is OFF")
  text = input("Do you want to turn the alarm system ON? (1=yes, 0=no)")
  if int(text)==1:
    setting['status']['ENABLE']='True'

with open(config_file, "w") as outfile:
  json.dump(setting, outfile, indent=4)

